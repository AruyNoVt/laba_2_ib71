def encrypt_caesar(key, text):
    res = ''
    for ch in text.upper():
        if ch.isalpha():
            code = ord(ch) + key
            if ch in 'QWERTYUIOPASDFGHJKLZXCVBNM':
                if code > ord('Z'):
                        res += (chr(ord('A') + ((code - ord('A')) % 26)))
                else:
                        res += (chr(code))
            if ch in 'ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ':
                if code > ord('Я'):
                        res += (chr(ord('А') + ((code - ord('А')) % 32)))
                else:
                        res += (chr(code))
        else:
            res += (ch)
    return res
 
def decrypt_caesar(key, text):
    res = ''
    for ch in text.upper():
        if ch.isalpha():
            code = ord(ch) - key
            if ch in 'QWERTYUIOPASDFGHJKLZXCVBNM':
                if code < ord('A'):
                    res += (chr(ord('Z') - ((ord('Z') - code) % 26)))
                else:
                    res += (chr(code))
            if ch in 'ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ':
                if code < ord('А'):
                    res += (chr(ord('Я') - ((ord('Я') - code) % 32)))
                else:
                    res += (chr(code))
        else:
            res += (ch)
    return res
 
if __name__ == '__main__':
    text = input('Что надобно ввести?: ')
    key = 3
    encrypt_text = encrypt_caesar(key, text)
    print('encrypt:', encrypt_text)
    print('decrypt:', decrypt_caesar(key, encrypt_text))